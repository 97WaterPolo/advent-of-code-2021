﻿using AOC2021.Models;
using AOC2021.Test.Models;

namespace AOC2021.Tests.Models
{
    public class TestRequest
    {
        public Answer Answer { get; set; }
        public AOCVersion Version { get; set; }
        public string Day { get; set; }
    }
}
