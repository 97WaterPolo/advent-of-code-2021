﻿using System;

namespace AOC2021.Test.Models
{
    public class Answer
    {
        public string Day_A_Test { get; set; }
        public string Day_A_Input { get; set; }
        public string Day_B_Test { get; set; }
        public string Day_B_Input { get; set; }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Answer a = (Answer)obj;
                string failedAnswer = string.Empty;
                if (!isValidAnswer(Day_A_Input, a.Day_A_Input))
                    throw new Exception($"Failed Day A Input ({Day_A_Input}) vs ({a.Day_A_Input})");
                if (!isValidAnswer(Day_A_Test, a.Day_A_Test))
                    throw new Exception($"Failed Day A Test ({Day_A_Test}) vs ({a.Day_A_Test})");

                if (!isValidAnswer(Day_B_Input, a.Day_B_Input))
                    throw new Exception($"Failed Day B Input ({Day_B_Input}) vs ({a.Day_B_Input})");
                if (!isValidAnswer(Day_B_Test, a.Day_B_Test))
                    throw new Exception($"Failed Day B Test ({Day_B_Test}) vs ({a.Day_B_Test})");

                if (string.IsNullOrEmpty(failedAnswer))
                {
                    return true;
                }
                Console.WriteLine(failedAnswer);
                return false;
            }
        }

        private bool isValidAnswer(string lInput, string input)
        {
            if (string.IsNullOrWhiteSpace(lInput) && string.IsNullOrWhiteSpace(input))
                return true;
            return lInput.Equals(input);
        }
    }
}
