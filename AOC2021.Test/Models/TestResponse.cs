﻿

using AOC2021.Models;
using AOC2021.Test.Models;

namespace AOC2021.Tests.Models
{
    public class TestResponse
    {
        public AOCVersion Version { get; set; }
        public Answer Answer { get; set; }
    }
}
