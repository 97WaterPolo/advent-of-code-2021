﻿using AOC2021.Models;
using AOC2021.Tests.Models;
using System;
using System.IO;
using System.Linq;

namespace AOC2021.Test
{
    public class AOCTester
    {
        public TestResponse Test(TestRequest request)
        {
            Console.WriteLine("Testing " + request.Day);
            var response = new TestResponse() { Answer = new Models.Answer() };

            var test = GetTextInput(request.Day, "test");
            var input = GetTextInput(request.Day, "input");

            Console.WriteLine($"Testing {request.Day} Part A, Test data");
            if (!string.IsNullOrEmpty(request.Answer.Day_A_Test))
                response.Answer.Day_A_Test = GetAOCDay(request.Day).ExecuteDay(new AOCRequest() { Input = test, Version = AOCVersion.A, IgnoreLogMessages = true }).Answer.ToString();
            Console.WriteLine(response.Answer.Day_A_Test);
            Console.WriteLine($"Testing {request.Day} Part A, Input data");
            if (!string.IsNullOrEmpty(request.Answer.Day_A_Input))
                response.Answer.Day_A_Input = GetAOCDay(request.Day).ExecuteDay(new AOCRequest() { Input = input, Version = AOCVersion.A, IgnoreLogMessages = true }).Answer.ToString();
            Console.WriteLine(response.Answer.Day_A_Input);

            Console.WriteLine($"Testing {request.Day} Part B, Test data");
            if (!string.IsNullOrEmpty(request.Answer.Day_B_Test))
                response.Answer.Day_B_Test = GetAOCDay(request.Day).ExecuteDay(new AOCRequest() { Input = test, Version = AOCVersion.B, IgnoreLogMessages = true }).Answer.ToString();
            Console.WriteLine(response.Answer.Day_B_Test);
            Console.WriteLine($"Testing {request.Day} Part B, Input data");
            if (!string.IsNullOrEmpty(request.Answer.Day_B_Input))
                response.Answer.Day_B_Input = GetAOCDay(request.Day).ExecuteDay(new AOCRequest() { Input = input, Version = AOCVersion.B, IgnoreLogMessages = true }).Answer.ToString();
            Console.WriteLine(response.Answer.Day_B_Input);
            return response;
        }

        private AOCDay GetAOCDay(string route)
        {
            AOCDay day = null;
            var type = typeof(AOCDay);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface && !p.IsAbstract);
            foreach (var x in types)
            {
                if (x.Name.ToLower() == route.ToLower())
                {
                    day = (AOCDay)(IAOCService)Activator.CreateInstance(x);
                }
            }
            Console.WriteLine($"Retrieving route for {route}. Found {day}");
            return (AOCDay)day;
        }

        private string GetTextInput(string day, string type)
        {
            var constructedFileName = $"Input{Path.DirectorySeparatorChar}{day}_{type}.txt";
            foreach (var file in Directory.GetFiles("Input"))
            {
                if (file.ToLower().Equals(constructedFileName.ToLower()))
                {
                    Console.WriteLine("Loading " + file);
                    return File.ReadAllText(file);
                }
            }
            throw new Exception($"No Text Input found for {day} {type}");
        }
    }
}
