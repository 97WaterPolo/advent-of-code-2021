﻿using AOC2021.Test;
using AOC2021.Tests.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AOC2021.Models;
using AOC2021.Test.Models;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AOC2021.Tests
{
    [TestClass]
    public class DayTest
    {
        private AOCTester _tester;
        public DayTest()
        {
            _tester = new AOCTester();
        }

        [TestMethod]
        public void Day1()
        {
            var request = new TestRequest() { Day = "day1", Answer = new Answer() { Day_A_Test = "7", Day_A_Input = "1759", Day_B_Test = "5", Day_B_Input = "1805" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        [TestMethod]
        public void Day2()
        {
            var request = new TestRequest() { Day = "day2", Answer = new Answer() { Day_A_Test = "150", Day_A_Input = "1762050", Day_B_Test = "900", Day_B_Input = "1855892637" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        [TestMethod]
        public void Day3()
        {
            var request = new TestRequest() { Day = "day3", Answer = new Answer() { Day_A_Test = "198", Day_A_Input = "3148794", Day_B_Test = "230", Day_B_Input = "2795310" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        [TestMethod]
        public void Day4()
        {
            var request = new TestRequest() { Day = "day4", Answer = new Answer() { Day_A_Test = "4512", Day_A_Input = "63424", Day_B_Test = "1924", Day_B_Input = "23541" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        [TestMethod]
        public void Day5()
        {
            var request = new TestRequest() { Day = "day5", Answer = new Answer() { Day_A_Test = "5", Day_A_Input = "7644", Day_B_Test = "12", Day_B_Input = "18627" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        [TestMethod]
        public void Day6()
        {
            var request = new TestRequest() { Day = "day6", Answer = new Answer() { Day_A_Test = "5934", Day_A_Input = "390011", Day_B_Test = "26984457539", Day_B_Input = "1746710169834" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }
        [TestMethod]
        public void Day7()
        {
            var request = new TestRequest() { Day = "day7", Answer = new Answer() { Day_A_Test = "37", Day_A_Input = "345035", Day_B_Test = "168", Day_B_Input = "97038163" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }
        [TestMethod]
        public void Day8()
        {
            var request = new TestRequest() { Day = "day8", Answer = new Answer() { Day_A_Test = "26", Day_A_Input = "543", Day_B_Test = "61229", Day_B_Input = "994266" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        [TestMethod]
        public void Day9()
        {
            var request = new TestRequest() { Day = "day9", Answer = new Answer() { Day_A_Test = "15", Day_A_Input = "577", Day_B_Test = "1134", Day_B_Input = "1069200" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        [TestMethod]
        public void Day10()
        {
            var request = new TestRequest() { Day = "day10", Answer = new Answer() { Day_A_Test = "26397", Day_A_Input = "168417", Day_B_Test = "288957", Day_B_Input = "2802519786" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        [TestMethod]
        public void Day11()
        {
            var request = new TestRequest() { Day = "day11", Answer = new Answer() { Day_A_Test = "1656", Day_A_Input = "1793", Day_B_Test = "195", Day_B_Input = "247" } };
            var result = _tester.Test(request);
            Assert.IsTrue(request.Answer.Equals(result.Answer));
        }

        //[TestMethod]
        public void MapVsEnumberable()
        {
            var addSw = new Stopwatch();
            addSw.Start();
            var eList = new System.Collections.Generic.List<DataPoint>();
            for (int x = 0; x < 1000; x++)
                for (int y = 0; y < 1000; y++)
                {
                    eList.Add(new DataPoint() { x = x, y = y, name = $"{x} -  {y}" });
                }
            addSw.Stop();
            Console.WriteLine($"Took {addSw.ElapsedMilliseconds}ms to create list");
            addSw.Restart();
            var dList = new Dictionary<string, DataPoint>();
            for (int x = 0; x < 1000; x++)
                for (int y = 0; y < 1000; y++)
                {
                    dList.Add($"{x},{y}", new DataPoint() { x = x, y = y, name = $"{x} -  {y}" });
                }
            addSw.Stop();
            Console.WriteLine($"Took {addSw.ElapsedMilliseconds}ms to create dictionary");

            var random = new Random();
            var getSW = new Stopwatch();
            getSW.Start();
            for (int i = 0; i < 1000; i++)
            {
                var pointX = random.Next(0, 1000);
                var pointY = random.Next(0, 1000);
                var point = GetDP(eList, pointX, pointY);
                Assert.AreEqual(pointY, point.y);
            }
            getSW.Stop();
            Console.WriteLine($"Took {getSW.ElapsedTicks}ticks to get list ");
            getSW.Reset();
            getSW.Start();
            for (int i = 0; i < 1000; i++)
            {
                var pointX = random.Next(0, 1000);
                var pointY = random.Next(0, 1000);
                var point = dList[$"{pointX},{pointY}"];
                Assert.AreEqual(pointY, point.y);
            }
            getSW.Stop();
            Console.WriteLine($"Took {getSW.ElapsedTicks}ticks to get dictionary ");
        }

        private DataPoint GetDP(List<DataPoint> eList, int pointX, int pointY)
        {
            foreach (var bs in eList)
                if (bs.x == pointX && bs.y == pointY)
                    return bs;
            return null;
        }

    }
    class DataPoint
    {
        public int x { get; set; }
        public int y { get; set; }
        public string name { get; set; }
    }
}
