﻿using AOC2021.Models;
using AOC2021.Models.Day5;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Days
{
    public class Day5 : AOCDay
    {
        private Dictionary<string, Vent> floor;
        public Day5()
        {
            floor = new Dictionary<string, Vent>();
        }
        protected override AOCResponse ExecutePartA()
        {
            var lines = GetSplitInput().Select(x => new Line(x));
            foreach (var line in lines)
            {
                var pointsOnLine = line.GetPointsInHorizVertLine();
                foreach (var point in pointsOnLine)
                {
                    GetVent(point.X, point.Y).OverlappingLines.Add(line);
                }
            }

            var overlap = floor.Values.Where(x => x.OverlappingLines.Count >= 2);
            foreach (var o in overlap)
            {
                Log($"Over laps at {o.X},{o.Y} count of {o.OverlappingLines.Count}");
            }
            this._response.Answer = overlap.Count().ToString();

            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            var lines = GetSplitInput().Select(x => new Line(x));
            foreach (var line in lines)
            {
                var pointsOnLine = line.GetPointsInAllDirections();
                foreach (var point in pointsOnLine)
                {
                    GetVent(point.X, point.Y).OverlappingLines.Add(line);
                }
            }

            var overlap = floor.Values.Where(x => x.OverlappingLines.Count >= 2);
            foreach (var o in overlap)
            {
                Log($"Over laps at {o.X},{o.Y} count of {o.OverlappingLines.Count}");
            }
            this._response.Answer = overlap.Count().ToString();
            return this._response;
        }

        private Vent GetVent(int x, int y)
        {
            Vent v;
            if (floor.TryGetValue($"{x},{y}", out v))
            {
                return v; //It found the vent
            }
            v = new Vent() { X = x, Y = y };
            floor.Add($"{x},{y}", v);
            return v;
        }

    }
}
