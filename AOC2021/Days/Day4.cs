﻿using AOC2021.Helper;
using AOC2021.Models;
using AOC2021.Models.Day4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Days
{
    public class Day4 : AOCDay
    {
        private static readonly int BOARD_SIZE = 5;
        private List<BingoBoard> _boards;
        public Day4()
        {
            _boards = new List<BingoBoard>();
        }
        protected override AOCResponse ExecutePartA()
        {
            var input = GetSplitInput();
            var checkedValues = input.First().Trim().Split(","); //Save selected answers into variable
            CreateBoards(input);

            BingoBoard winner = null;
            foreach (var ball in checkedValues)
            {
                Log("Ball picked was " + ball);
                foreach (var board in _boards)
                {
                    board.MarkValueAsChecked(ball.ToInt());
                    if (board.IsWinner())
                    {
                        winner = board;
                        Log("We have a winning board, number " + winner.Id);
                        break;
                    }
                }
                if (winner != null)
                    break;
            }
            this._response.Answer = winner.CalculateScore().ToString();
            Log("Winning board score is " + this._response.Answer);

            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            var input = GetSplitInput();
            var checkedValues = input.First().Trim().Split(","); //Save selected answers into variable
            CreateBoards(input);

            BingoBoard winner = null;
            foreach (var ball in checkedValues)
            {
                Log("Ball picked was " + ball);
                foreach (var board in _boards.Reverse<BingoBoard>())
                {
                    board.MarkValueAsChecked(ball.ToInt());
                    if (board.IsWinner())
                    {
                        winner = board;
                        _boards.Remove(board);
                    }
                }
            }
            this._response.Answer = winner.CalculateScore().ToString();
            Log("Winning board score is " + this._response.Answer);
            return this._response;
        }

        private void CreateBoards(string[] input)
        {
            //Create all of our game boards
            int skipValue = 2;
            while (skipValue < input.Length)
            {
                var group = input.Skip(skipValue).Take(5);
                _boards.Add(new BingoBoard(BOARD_SIZE, group));
                skipValue += 6;
            }

        }
    }
}
