﻿using AOC2021.Helper;
using AOC2021.Models;
using AOC2021.Models.Day6;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AOC2021.Days
{
    public class Day6 : AOCDay
    {
        private int MODELING_DAYS = 80;
        private List<Fish> _fishes;
        public Day6()
        {
            this._fishes = new List<Fish>();
        }

        /// <summary>
        /// First implementaiton uses a list and a fish class to keep track. Each fish is independent of one another
        /// and we just sum up the number of fish objects we have regardless of the day that they are on.
        /// </summary>
        /// <returns></returns>
        protected override AOCResponse ExecutePartA()
        {
            var initialState = this._request.Input.Trim().Split(",");
            foreach (var day in initialState)
                _fishes.Add(new Fish(day.ToInt()));

            for (int i = 0; i < MODELING_DAYS; i++)
            {
                var newFishes = new List<Fish>();
                foreach (var fish in _fishes)
                { 
                    var child = fish.DayPassed();
                    if (child != null)
                        newFishes.Add(child);
                }
                foreach (var newFish in newFishes)
                    _fishes.Add(newFish);
                Log($"Day {i + 1} Count {_fishes.Count}:  " + 
                   $"{_fishes.Where(x => x.Count == 0).Count()}, " +
                   $"{_fishes.Where(x => x.Count == 1).Count()}, " +
                   $"{_fishes.Where(x => x.Count == 2).Count()}, " +
                   $"{_fishes.Where(x => x.Count == 3).Count()}, " +
                   $"{_fishes.Where(x => x.Count == 4).Count()}, " +
                   $"{_fishes.Where(x => x.Count == 5).Count()}, " +
                   $"{_fishes.Where(x => x.Count == 6).Count()}, " +
                   $"{_fishes.Where(x => x.Count == 7).Count()}, " +
                   $"{_fishes.Where(x => x.Count == 8).Count()}, ");
            }

            this._response.Answer = _fishes.Count.ToString();

            return this._response;
        }

        /// <summary>
        /// More efficient way, have long[] array of 9 values (0-8). Each day that passes we move the
        /// value from the later day into the previous day. When fish hits day 0 we do two things
        ///     1) Add the number of 0 fishes back to day 6 (mothers returning back into circulation)
        ///     2) Add the number of 0 fishes to day 8 indicating that they are babies.
        /// This is a vastly more efficient way as we only keep track of the number of fishes per day, not
        /// the other way around of days per fishies.
        /// </summary>
        /// <returns></returns>
        protected override AOCResponse ExecutePartB()
        {
            MODELING_DAYS = 256;

            var fishSchool = new FishSchool(this._request.Input.Trim()); //Create a new school of fish

            for (int i = 0; i < MODELING_DAYS; i++)
            {
                fishSchool.PassDay();
                Log($"Day {i+1} Count {fishSchool.NumberOfFishies()}:  " + fishSchool.ToString());
            }
            this._response.Answer = fishSchool.NumberOfFishies().ToString();
            return this._response;
        }
    }
}
