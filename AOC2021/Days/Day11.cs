﻿using AOC2021.Models;
using AOC2021.Models.Day11;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Days
{
    public class Day11 : AOCDay
    {
        private static readonly int BOARD_SIZE = 10;
        public Octopus[,] _board;
        public Day11()
        {
            this._board = new Octopus[BOARD_SIZE, BOARD_SIZE];
        }
        protected override AOCResponse ExecutePartA()
        {
            CreateOctoBoard();
            int totalFlashes = 0;
            PrintBoard("0");
            for (int i = 1; i <= 100; i++)
            {
                //Initial energy assumption
                for (int x = 0; x < BOARD_SIZE; x++)
                {
                    for (int y = 0; y < BOARD_SIZE; y++)
                    {
                        _board[x, y].IncreaseEnergy(i);
                    }
                }
                //Energy propagation with the flash
                totalFlashes += Propagate(i);
                PrintBoard(i.ToString());
            }
            this.Answer = totalFlashes;
            return this._response;
        }


        protected override AOCResponse ExecutePartB()
        {
            CreateOctoBoard();
            int totalFlashes = 0;
            PrintBoard("0");
            int i = 1;
            while(true)
            {
                if (DidAllFlash())
                {
                    Log("Found case where all Octopus flashed at " + (i - 1));
                    this.Answer = i - 1;
                    break;
                }
                //Initial energy assumption
                for (int x = 0; x < BOARD_SIZE; x++)
                {
                    for (int y = 0; y < BOARD_SIZE; y++)
                    {
                        _board[x, y].IncreaseEnergy(i);
                    }
                }
                //Energy propagation with the flash
                totalFlashes += Propagate(i);
                PrintBoard(i.ToString());
                i++;
            }
            return this._response;
        }


        private int Propagate(int step)
        {
            int totalFlashes = 0;
            while (AnyLeftToFlash(step))
            {
                for (int x = 0; x < BOARD_SIZE; x++)
                {
                    for (int y = 0; y < BOARD_SIZE; y++)
                    {
                        if (_board[x, y].CanFlash(step))
                        {
                            _board[x, y].Flash(step);
                            totalFlashes++;
                            foreach (var t in GetSurroundingCoords(x, y))
                                _board[t.Item1, t.Item2].IncreaseEnergy(step);
                        }
                    }
                }
            }
            return totalFlashes;
        }

        public bool AnyLeftToFlash(int step)
        {
            bool flash = false;
            for (int x = 0; x < BOARD_SIZE; x++)
            {
                for (int y = 0; y < BOARD_SIZE; y++)
                {
                    if (_board[x, y].CanFlash(step))
                    {
                        flash = true;
                        break;
                    }
                }
            }
            return flash;
        }

        private bool DidAllFlash()
        {
            for (int x = 0; x < BOARD_SIZE; x++)
            {
                for (int y = 0; y < BOARD_SIZE; y++)
                {
                    if (_board[x,y].EnergyLevel != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void PrintBoard(string s)
        {
            Log("Step " + s);
            for (int i = 0; i < BOARD_SIZE; i++)
            {
                string line = "";
                for (int j = 0; j < BOARD_SIZE; j++)
                {
                    line += _board[i, j] + " ";
                }
                Log(line);
            }
        }

        private void CreateOctoBoard()
        {
            int row = 0;
            foreach (var line in GetSplitInput())
            {
                for (int col = 0; col < line.Length; col++)
                {
                    _board[row, col] = new Octopus(Convert.ToInt32(line[col].ToString()));
                }
                row++;
            }
        }

        private List<Tuple<int,int>> GetSurroundingCoords(int x, int y)
        {
            var positions = new List<Tuple<int, int>>();
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                    if ((x + i >= 0 && x + i < BOARD_SIZE)
                        && (y + j >= 0 && y + j < BOARD_SIZE))
                        positions.Add(Tuple.Create(x + i, y + j));
            }
            return positions;
        }
    }
}
