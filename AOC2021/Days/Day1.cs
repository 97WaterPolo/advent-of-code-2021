﻿using AOC2021.Models;
using AOC2021.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Days
{
    public class Day1 : AOCDay
    {
        protected override AOCResponse ExecutePartA()
        {
            var input = GetSplitInput();
            int increase = 0;
            for (int i = 0; i < input.Length-1; i++)
            {
                var curret = input[i].ToInt();
                var next = input[i + 1].ToInt();
                if (next > curret)
                {
                    increase++;
                    Log("Increase");
                }
            }
            this._response.Answer = increase.ToString();
            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            var input = GetSplitInput();
            var list = new List<int>();
            for (int i = 0; i < input.Length-2; i++)
            {
                var a = input[i].ToInt();
                var b = input[i + 1].ToInt();
                var c = input[i + 2].ToInt();
                list.Add(a + b + c);
            }
            list.ForEach(x => Log(x.ToString()));

            int increase = 0;
            for (int i = 0; i < list.Count-1; i++)
            {
                if (list[i + 1] > list[i])
                    increase++;
            }
            this._response.Answer = increase.ToString();
            return this._response;
        }
    }
}
