﻿using AOC2021.Helper;
using AOC2021.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Days
{
    public class Day7 : AOCDay
    {
        private int[] _crabs;
        protected override AOCResponse ExecutePartA()
        {
            CreateCrabs();
            int minFuel = int.MaxValue;
            for (int position = _crabs.Min(); position < _crabs.Max(); position++)
            {
                int fuel = 0;
                for (int i = 0; i < _crabs.Length; i++)
                {
                    fuel += Math.Abs(position - _crabs[i]);
                }
                minFuel = Math.Min(minFuel, fuel);
                Log($"For pos {position} it took {fuel}");
            }
            this._response.Answer = minFuel.ToString();
            Log("Min Fuel is " + minFuel);
            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            CreateCrabs();
            int minFuel = int.MaxValue;
            for (int position = _crabs.Min(); position < _crabs.Max(); position++)
            {
                int fuel = 0;
                for (int i = 0; i < _crabs.Length; i++)
                {
                    var spacesMoved= Math.Abs(position - _crabs[i]);
                    var fuelConsumed = ((spacesMoved * spacesMoved) + spacesMoved) / 2; //(n^2+n)/2
                    fuel += fuelConsumed;
                }
                minFuel = Math.Min(minFuel, fuel);
                Log($"For pos {position} it took {fuel}");
            }
            this._response.Answer = minFuel.ToString();
            Log("Min Fuel is " + minFuel);
            return this._response;
        }

        private void CreateCrabs()
        {
            var crabPositions = this._request.Input.Trim().Split(",");
            _crabs = new int[crabPositions.Length];
            for (int i = 0; i < crabPositions.Length; i++)
                _crabs[i] = crabPositions[i].ToInt();
        }
    }
}
