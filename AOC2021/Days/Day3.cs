﻿using AOC2021.Models;
using AOC2021.Models.Day3;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AOC2021.Days
{
    public class Day3 : AOCDay
    {
        protected override AOCResponse ExecutePartA()
        {
            var numbers = new List<BinaryNumber>();
            foreach (var bNum in GetSplitInput())
            {
                numbers.Add(new BinaryNumber(bNum));
            }

            var length = numbers.FirstOrDefault().Size();
            var gamma = string.Empty;
            for (int i = 0; i < length; i++)
            {
                int zero = 0;
                int one = 0;
                foreach (var bNum in numbers)
                {
                    if (bNum.GetIntAt(i) == 0)
                        zero++;
                    else
                        one++;
                }
                Log($"Bit {i} is {zero} or {one}");
                gamma += (zero > one ? "0" : "1").ToString();
            }
            var epsilon = string.Concat(gamma.Select(x => x == '0' ? '1' : '0')); //Not of gamma
            Log($"Gamma rate is {gamma} or {ToInt(gamma)}");
            Log($"Episode rate is {epsilon} or {ToInt(epsilon)}");
            this._response.Answer = (ToInt(gamma) * ToInt(epsilon)).ToString();
            Log($"Final answer is {this._response.Answer}");

            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            var initialNumbers = GetSplitInput().Select(x => new BinaryNumber(x)).ToList();
            Log($"An initial count of numbers is {initialNumbers.Count}");

            int count = 0;
            var oxygenRate = FilterOutBinary(true, count, initialNumbers);
            while (oxygenRate.Count > 1)
            {
                oxygenRate = FilterOutBinary(true, ++count, oxygenRate);
                Log($"Oxygen rate has {oxygenRate.Count} numbers left");
            }
                

            count = 0;
            var co2Rate = FilterOutBinary(false, count, initialNumbers);
            while (co2Rate.Count > 1)
            {
                co2Rate = FilterOutBinary(false, ++count, co2Rate);
                Log($"CO2 rate has {co2Rate.Count} numbers left");
            }

            Log("Oxygen rate is " + oxygenRate.FirstOrDefault().ToInt());
            Log("CO2 rate is " + co2Rate.FirstOrDefault().ToInt());
            this._response.Answer = (oxygenRate.FirstOrDefault().ToInt() * co2Rate.FirstOrDefault().ToInt()).ToString();
            return this._response;
        }

        public List<BinaryNumber> FilterOutBinary(bool mostCommon, int selectedBit, List<BinaryNumber> nums)
        {
            var zeroNums = new List<BinaryNumber>();
            var oneNums = new List<BinaryNumber>();
            foreach (var num in nums)
            {
                if (num.GetIntAt(selectedBit) == 0)
                    zeroNums.Add(num);
                else
                    oneNums.Add(num);
            }
            //If the zero count is greater return zeroNums, if equal or one count is higher return oneNums
            if (mostCommon)
                return zeroNums.Count > oneNums.Count ? zeroNums : oneNums;
            else
                return zeroNums.Count <= oneNums.Count ? zeroNums : oneNums;
        }
        public int ToInt(string s)
        {
            return Convert.ToInt32(s, 2);
        }
    }
}
