﻿using AOC2021.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Days
{
    public class Day9 : AOCDay
    {
        private int[][] _board;
        private int _rowCount, _colCount;
        protected override AOCResponse ExecutePartA()
        {
            CreateBoard();
            int riskCount = 0;
            var lowPoints = FindLowPoints();
            foreach (var lP in lowPoints)
            {
                riskCount += 1 + _board[lP.Item1][lP.Item2];
            }

            this._response.Answer = riskCount;
            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            CreateBoard();
            var lowPoints = FindLowPoints();
            var basinCount = new List<int>();
            foreach (var lP in lowPoints)
            {
                var basin = GetBasin(lP.Item1, lP.Item2);
                basinCount.Add(basin.Count());
                Log($"LP: {lP.Item1},{lP.Item2} has a basin size of {basin.Count()}");
                Log(string.Join(" ", basin.Select(bP => "(" + bP.Item1 + ", " + bP.Item2 + ")")));
            }
            //Order by int then take the top 3, and then multiple them together.
            var product = basinCount.OrderByDescending(x => x).Take(3).Aggregate((total, next) => total * next);
            this._response.Answer = product;
            return this._response;
        }

        private List<Tuple<int,int>> FindLowPoints()
        {
            var lowPoints = new List<Tuple<int, int>>();
            for (int x = 0; x < _rowCount; x++)
            {
                for (int y = 0; y < _colCount; y++)
                {
                    var surround = GetSurroundingPositions(x, y);
                    bool isLowest = true;
                    foreach (var t in surround)
                    {
                        if (_board[x][y] >= _board[t.Item1][t.Item2])
                            isLowest = false;
                    }
                    if (isLowest)
                    {
                        lowPoints.Add(Tuple.Create(x, y));
                        Log($"{x},{y} is a low point with value {_board[x][y]}!");
                    }

                }
            }
            return lowPoints;
        }

        private List<Tuple<int,int>> GetSurroundingPositions(int row, int column)
        {
            var positions = new List<Tuple<int, int>>();
            for (int i = -1; i <= 1; i+=2)
            {
                if (row + i < _rowCount && row + i >= 0)
                    positions.Add(new Tuple<int, int>(row + i, column));
                if (column + i < _colCount && column + i >= 0)
                    positions.Add(new Tuple<int, int>(row, column + i));
            }
            return positions;
        }

        private List<Tuple<int,int>> GetBasin(int x, int y)
        {
            List<Tuple<int, int>> basin = new List<Tuple<int, int>>();
            AddBasin(Tuple.Create(x, y), basin);
            return basin.Distinct().ToList();
        }

        private void AddBasin(Tuple<int,int> point, List<Tuple<int,int>> list)
        {
            foreach (var t in GetSurroundingPositionsThatAreNotNine(point.Item1, point.Item2).Except(list))
            {
                list.Add(t);
                AddBasin(t, list);
            }
        }

        private IEnumerable<Tuple<int, int>> GetSurroundingPositionsThatAreNotNine(int row, int column)
        {
            var positions = GetSurroundingPositions(row, column);
            return positions.Where(x => _board[x.Item1][x.Item2] != 9);
        }

        private void CreateBoard()
        {
            var rows = new List<int[]>();
            foreach (var line in GetSplitInput())
            {
                var columns = new List<int>();
                foreach (var c in line)
                {
                    columns.Add(Convert.ToInt32(c.ToString()));
                }
                rows.Add(columns.ToArray());
            }
            _colCount = rows[0].Length;
            _rowCount = rows.Count;
            _board = rows.ToArray();
        }
    }
}
