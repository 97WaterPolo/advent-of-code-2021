﻿using AOC2021.Models;
using AOC2021.Models.Day10;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Days
{
    public class Day10 : AOCDay
    {
        private List<ChunkReaderResult> _results;
        public Day10()
        {
            _results = new List<ChunkReaderResult>();
        }
        protected override AOCResponse ExecutePartA()
        {
            foreach (var line in GetSplitInput())
            {
                _results.Add(new ChunkReader(line).Process());
            }
            var illegalScore = 0;
            illegalScore += _results.Where(x => x.isCorrupted && x.CorruptedChar == ')').Count() * 3;
            illegalScore += _results.Where(x => x.isCorrupted && x.CorruptedChar == ']').Count() * 57;
            illegalScore += _results.Where(x => x.isCorrupted && x.CorruptedChar == '}').Count() * 1197;
            illegalScore += _results.Where(x => x.isCorrupted && x.CorruptedChar == '>').Count() * 25137;
            this._response.Answer = illegalScore;
            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            foreach (var line in GetSplitInput())
            {
                _results.Add(new ChunkReader(line).Process());
            }
            var incomplete = _results.Where(x => x.isIncomplete);
            var totalPoints = new List<long>();
            foreach (var inc in incomplete)
            {
                var total = 0L;
                foreach (var c in inc.CompletionString)
                {
                    total *= 5;
                    total += GetPointValueForCompletion(c);
                }
                totalPoints.Add(total);
            }
            this.Answer = totalPoints.OrderBy(x => x).ToArray()[totalPoints.Count / 2];
            return this._response;
        }

        private int GetPointValueForCompletion(char c)
        {
            switch (c)
            {
                case ')':
                    return 1;
                case ']':
                    return 2;
                case '}':
                    return 3;
                case '>':
                    return 4;
            }
            throw new Exception("Invalid char for Part B: " + c);
        }
    }
}
