﻿using AOC2021.Models;
using AOC2021.Models.Day2;

namespace AOC2021.Days
{
    public class Day2 : AOCDay
    {
        private Submarine _sub;
        public Day2()
        {
            _sub = new Submarine();
        }
        protected override AOCResponse ExecutePartA()
        {
            var actions = GetSplitInput();
            foreach (var action in actions)
            {
                _sub.Move(action, this._request.Version);
            }
            Log("Depth is " + _sub.Depth);
            Log("Horiz is " + _sub.Forward);
            this._response.Answer = ((_sub.Depth) * _sub.Forward).ToString();
            this._response.Status = true;
            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            return ExecutePartA();
        }
    }
}
