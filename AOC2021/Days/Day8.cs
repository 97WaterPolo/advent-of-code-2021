﻿using AOC2021.Models;
using AOC2021.Models.Day8;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Days
{
    public class Day8 : AOCDay
    {
        protected override AOCResponse ExecutePartA()
        {
            var count = 0;
            var uniqueNumberOfLineSegments = new int[] { 2, 4, 3, 7 };
            var lines = GetSplitInput();
            foreach (var line in lines)
            {
                var displayedDigits = line.Split("|")[1].Trim();
                foreach (var digit in displayedDigits.Split(" "))
                {
                    if (uniqueNumberOfLineSegments.Contains(digit.Trim().Length))
                    {
                        Log($"{digit} has a length of {digit.Length} which is unique.");
                        count++;
                    }
                }
            }
            this._response.Answer = count;
            return this._response;
        }

        protected override AOCResponse ExecutePartB()
        {
            var sum = 0;
            foreach (var line in GetSplitInput())
            {
                var inputNumbers = line.Split("|")[0].Trim();
                var outputDigits = line.Split("|")[1].Trim();
                var ssdGenerator = new SSDGenerator(inputNumbers);
                ssdGenerator.Generate();
                var digits = ssdGenerator.ConvertToNumbers(outputDigits);
                sum += Convert.ToInt32(digits);
            }
            this._response.Answer = sum;
            return this._response;
        }
    }
}
