﻿using AOC2021.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AOC2021.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdventOfCodeController : ControllerBase
    {
        private readonly ILogger<AdventOfCodeController> _logger;

        public AdventOfCodeController(ILogger<AdventOfCodeController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        [Consumes("text/plain")]
        [Route("day1")]
        [Route("day2")]
        [Route("day3")]
        [Route("day4")]
        [Route("day5")]
        [Route("day6")]
        [Route("day7")]
        [Route("day8")]
        [Route("day9")]
        [Route("day10")]
        [Route("day11")]
        public AOCResponse Day(AOCVersion version, [FromBody] string input, bool IgnoreLogMessages = false)
        {
            AOCRequest request = new AOCRequest() { Input = input, Version = version, IgnoreLogMessages = IgnoreLogMessages };
            var splitRoute = this.ControllerContext.HttpContext.Request.Path.ToString().Split("/");
            string requestedRoute = splitRoute[splitRoute.Length - 1]; //Get the last endpoint value.
            return GetAOCDay(requestedRoute).ExecuteDay(request);
        }

        private AOCDay GetAOCDay(string route)
        {
            AOCDay day = null;
            var type = typeof(AOCDay);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface && !p.IsAbstract);
            foreach (var x in types)
            {
                if (x.Name.ToLower() == route.ToLower())
                {
                    day = (AOCDay) (IAOCService)Activator.CreateInstance(x);
                    day.SetLogger(this._logger);
                }


            }
            return (AOCDay) day;
        }
    }
}
