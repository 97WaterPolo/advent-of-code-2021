﻿using AOC2021.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace AOC2021.Models.Day5
{
    public class Line
    {
        public string LineEntry { get; set; }
        public Point Starting { get; set; }
        public Point Ending { get; set; }

        public Line(string line)
        {
            this.LineEntry = line;
            var split = line.Split("->");

            var xValue = split[0].Trim().Split(",");
            var yValue = split[1].Trim().Split(",");

            Starting = new Point(xValue[0].ToInt(), xValue[1].ToInt());
            Ending = new Point(yValue[0].ToInt(), yValue[1].ToInt());
        }

        public List<Point> GetPointsInHorizVertLine()
        {
            var list = new List<Point>();
            if (Starting.X == Ending.X)
            {
                var start = Math.Min(Starting.Y, Ending.Y);
                var end = Math.Max(Starting.Y, Ending.Y);
                for (int i = start; i <= end; i++)
                    list.Add(new Point(Starting.X, i));
            }
            else if (Starting.Y == Ending.Y)
            {
                var start = Math.Min(Starting.X, Ending.X);
                var end = Math.Max(Starting.X, Ending.X);
                for (int i = start; i <= end; i++)
                    list.Add(new Point(i, Starting.Y));
            }

            return list;
        }

        public List<Point> GetPointsInAllDirections()
        {
            var list = GetPointsInHorizVertLine();
            if (list.Count != 0)
                return list;

            var m = (Ending.Y - Starting.Y) / (Ending.X - Starting.X);
            var b = Ending.Y - (m * Ending.X);

            int minX = Math.Min(Starting.X, Ending.X);
            int maxX = Math.Max(Starting.X, Ending.X);
            for (int x = minX; x <= maxX; x++)
            {
                var y = (m * x) + b;
                list.Add(new Point(x, y));
            }
            return list;
        }
    }
}
