﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Models.Day5
{
    public class Vent
    {
        public int X { get; set; }
        public int Y { get; set; }

        public List<Line> OverlappingLines { get; set; }

        public Vent()
        {
            this.OverlappingLines = new List<Line>();
        }
    }
}
