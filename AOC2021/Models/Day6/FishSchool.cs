﻿using AOC2021.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Models.Day6
{
    public class FishSchool
    {
        public long[] _fishes;
        public FishSchool(string initialState)
        {
            _fishes = new long[9];
            foreach (var day in initialState.Split(","))
            {
                _fishes[day.ToInt()] += 1;
            }
        }

        public void PassDay()
        {
            var motherFishies = _fishes[0];
            for (int i = 1; i < _fishes.Length; i++)
            {
                _fishes[i - 1] = _fishes[i];
            }

            _fishes[6] += motherFishies; //Mothers go back to 6 days
            _fishes[8] = motherFishies; //These are the children

        }

        public override string ToString()
        {
            return string.Join(", ", _fishes);
        }

        public long NumberOfFishies()
        {
            long val = 0;
            foreach (var fish in _fishes)
            {
                val += fish;
            }
            return val;
        }
    }
}
