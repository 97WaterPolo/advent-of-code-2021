﻿namespace AOC2021.Models.Day6
{
    public class Fish
    {
        public int Count { get; set; }
        public Fish (int count)
        {
            this.Count = count;
        }

        public Fish DayPassed()
        {
            if (Count == 0)
            {
                Count = 6;
                return new Fish(8);
            }
            Count--;
            return null;
        }
    }
}
