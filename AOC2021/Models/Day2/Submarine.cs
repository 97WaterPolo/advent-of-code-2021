﻿namespace AOC2021.Models.Day2
{
    using System;
    using AOC2021.Helper;
    public class Submarine
    {
        public int Depth { get; set; }
        public int Forward { get; set; }

        public int Aim { get; set; }

        public void Move(string action, AOCVersion version)
        {
            var direction = action.Split(" ")[0];
            var value = action.Split(" ")[1];
            var subDir = Enum.Parse<SubmarineAction>(direction.ToUpper());
            if (version == AOCVersion.A)
                HandlePartA(value, subDir);
            else
                HandlePartB(value, subDir);
        }

        private void HandlePartA(string value, SubmarineAction subDir)
        {
            switch (subDir)
            {
                case SubmarineAction.FORWARD:
                    Forward += value.ToInt();
                    break;
                case SubmarineAction.DOWN:
                    Depth += value.ToInt();
                    break;
                case SubmarineAction.UP:
                    Depth -= value.ToInt();
                    break;
            }
        }

        private void HandlePartB(string value, SubmarineAction subDir)
        {
            switch (subDir)
            {
                case SubmarineAction.FORWARD:
                    Forward += value.ToInt();
                    Depth +=  (Aim * value.ToInt());
                    break;
                case SubmarineAction.DOWN:
                    Aim += value.ToInt();
                    break;
                case SubmarineAction.UP:
                    Aim -= value.ToInt();
                    break;
            }
        }
    }
}
