﻿
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AOC2021.Models
{
    public abstract class AOCDay : IAOCService
    {
        protected AOCRequest _request;
        protected AOCResponse _response;
        protected ILogger _logger;
        private List<string> _debugMessages;
        protected object Answer { set { this._response.Answer = value; } }
        public AOCDay()
        {
            this._response = new AOCResponse()
            {
                Status = false
            };
            this._debugMessages = new List<string>();
        }
        public AOCResponse ExecuteDay(AOCRequest request)
        {
            _request = request;
            var timer = new Stopwatch();
            try
            {
                timer.Start();
                switch (request.Version)
                {
                    case AOCVersion.A:
                        this._response = ExecutePartA();
                        break;
                    case AOCVersion.B:
                        this._response = ExecutePartB();
                        break;
                }
                timer.Stop();
                this._response.RunTime = timer.ElapsedTicks.ToString();
                this._response.Status = true;
            }
            catch (Exception e)
            {
                this._response.Answer = e.Message;
                this._response.Status = false;
                this._response.StackTrace = e.StackTrace;
            }
            this._response.Debug = this._debugMessages;
            return this._response;
        }

        protected abstract AOCResponse ExecutePartA();
        protected abstract AOCResponse ExecutePartB();

        protected string[] GetSplitInput()
        {
            return this._request.Input.Trim().Replace("\r", "").Split("\n");
        }
        public void SetLogger(ILogger logger)
        {
            this._logger = logger;
        }

        protected void Log(string message, params object[] args)
        {
            if (_logger != null && !this._request.IgnoreLogMessages)
                _logger.LogInformation(message, args);
            _debugMessages.Add(string.Format(message, args));
        }
    }
}
