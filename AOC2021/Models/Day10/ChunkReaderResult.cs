﻿
namespace AOC2021.Models.Day10
{
    public class ChunkReaderResult
    {
        public string Line { get; set; }
        public bool isValid { get; set; }
        public bool isIncomplete { get; set; }
        public string CompletionString { get; set; }
        public bool isCorrupted { get; set; }
        public char CorruptedChar { get; set; }
        public char ExpectedChar { get; set; }
    }
}
