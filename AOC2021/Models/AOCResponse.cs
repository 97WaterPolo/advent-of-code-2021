﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace AOC2021.Models
{
    [DataContract]
    [Serializable]
    public class AOCResponse
    {
        [DataMember]
        public DateTime Date { get { return DateTime.Now; } }
        [DataMember]
        public object Answer { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public IEnumerable<string> Debug { get; set; }
        [DataMember]
        public string StackTrace { get; set; }
        private string timeInTicks;
        [DataMember]
        public string RunTime { get { return FormatRunTime(); } set { timeInTicks = value; } }

        public string FormatRunTime()
        {
            var ts = TimeSpan.FromTicks((long)Convert.ToDouble(timeInTicks));
            var microseconds = (ts.Ticks - (ts.Milliseconds * TimeSpan.TicksPerMillisecond)) / (TimeSpan.TicksPerMillisecond / 1000);
            return $"Run time is {ts.Minutes}min {ts.Seconds}sec {ts.Milliseconds}ms {microseconds}µs";
        }
    }


}
