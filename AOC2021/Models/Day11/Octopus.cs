﻿
namespace AOC2021.Models.Day11
{
    public class Octopus
    {
        public Octopus(int initialEnergy)
        {
            this.EnergyLevel = initialEnergy;
        }
        public int LastFlashedStep { get; set; }
        public int EnergyLevel { get; set; }
        public void IncreaseEnergy(int step)
        {
            if (step != LastFlashedStep)
                EnergyLevel++;
        }
        
        public bool CanFlash(int step)
        {
            if (EnergyLevel > 9 && step != LastFlashedStep)
            {
                return true;
            }
            return false;
        }

        public void Flash(int step)
        {
            LastFlashedStep = step;
            EnergyLevel = 0;
        }

        public override string ToString()
        {
            return EnergyLevel.ToString();
        }

    }
}
