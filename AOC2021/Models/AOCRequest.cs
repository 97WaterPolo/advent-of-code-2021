﻿using System;
using System.Runtime.Serialization;

namespace AOC2021.Models
{
    [DataContract]
    [Serializable]
    public class AOCRequest
    {
        [DataMember]
        public AOCVersion Version { get; set; }
        [DataMember]
        public string Input { get; set; }
        [DataMember]
        public bool IgnoreLogMessages { get; set; }
    }
}
