﻿using System;

namespace AOC2021.Models.Day3
{
    public class BinaryNumber
    {
        private string _val;
        public BinaryNumber(string s)
        {
            _val = s;
        }

        public int GetIntAt(int pos)
        {
            return Convert.ToInt32(_val[pos].ToString());
        }

        public int Size()
        {
            return _val.Length;
        }

        public int ToInt()
        {
            return Convert.ToInt32(_val, 2);
        }
    }
}
