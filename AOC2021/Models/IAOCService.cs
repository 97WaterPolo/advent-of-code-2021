﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Models
{
    public interface IAOCService
    {
        public AOCResponse ExecuteDay(AOCRequest request);
    }
}
