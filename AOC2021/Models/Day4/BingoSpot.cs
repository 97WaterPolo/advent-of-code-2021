﻿namespace AOC2021.Models.Day4
{
    public class BingoSpot
    {
        public int Value { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool Checked { get; set; }
    }
}
