﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOC2021.Models.Day4
{
    public class BingoBoard
    {
        private static int ID_COUNT = 1;
        public int Id;
        private int _size;
        private List<BingoSpot> _board;
        private int _lastAnswer;
        public BingoBoard(int size, IEnumerable<string> values)
        {
            Id = ID_COUNT++;
            _size = size;
            _board = new List<BingoSpot>();
            ConstructBoard(values);
        }

        private void ConstructBoard(IEnumerable<string> values)
        {
            for (int x = 0; x < values.Count(); x++)
            {
                var splitValue = values.ElementAt(x).Trim().Replace("  ", " ").Split(" ");
                for (int y = 0; y < splitValue.Length; y++)
                {
                    _board.Add(new BingoSpot()
                    {
                        Checked = false,
                        Value = Convert.ToInt32(splitValue[y]),
                        X = x,
                        Y = y
                    });
                }
            }
        }

        private BingoSpot SpotAt(int x, int y)
        {
            foreach (var bs in _board)
                if (bs.X == x && bs.Y == y)
                    return bs;
            throw new Exception($"Can't find spot {x},{y} in the bingo board!");
        }

        public void MarkValueAsChecked(int value)
        {

            foreach (var bs in _board)
                if (bs.Value == value)
                {
                    bs.Checked = true;
                }
            _lastAnswer = value;
        }

        public string PrintBoard()
        {
            var boardPrintout = string.Empty;
            for (int x = 0; x < _size; x++)
            {
                var line = string.Empty;
                for (int y = 0; y < _size; y++)
                {
                    line += $"{SpotAt(x, y).Value} ";
                }
                boardPrintout += $"{line}{Environment.NewLine}";
            }
            return boardPrintout;
        }

        public bool IsWinner()
        {
            for (int x = 0; x < _size; x++)
            {
                //If its still checked, and the next spot is NOT checked set to false and ends the check
                bool horizontalRow = true;
                bool verticalRow = true;
                for (int y = 0; y < _size; y++)
                {
                    if (horizontalRow && !SpotAt(x, y).Checked)
                        horizontalRow = false;
                    if (verticalRow && !SpotAt(y, x).Checked)
                        verticalRow = false;
                }
                if (horizontalRow || verticalRow)
                    return true;
            }
            return false;
        }

        public int CalculateScore()
        {
            int unMarked = 0;
            foreach (var x in _board)
                if (!x.Checked)
                    unMarked += x.Value;
            return unMarked * _lastAnswer;
        }
    }
}
