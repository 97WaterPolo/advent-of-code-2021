﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace AOC2021.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AOCVersion
    {
        [EnumMember(Value = "A")]
        A,
        [EnumMember(Value = "B")]
        B
    }
}
