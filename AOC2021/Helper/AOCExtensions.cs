﻿using System;

namespace AOC2021.Helper
{
    public static class AOCExtensions
    {
        public static int ToInt(this string str)
        {
            return Convert.ToInt32(str);
        }
    }
}
